/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// Global variables
var window;
var windowHeight;
var windowWidth;
var content_right;
var content;
var content_admin_model;
var menu_left;

$(document).ready(function() {
    
    //Init
    initVar();
    initStucture();

    // Resize;
    $(window).resize(function() {
        initStucture();

    });

    //Manage menu
    $("#nav_model>ul>li>span").click(function() {
        openMenu($(this));
    });
});

// Init global variables
function initVar() {
    content_right = $('#content_right');
    content = $('#content');
    content_admin_model = $('#content_admin_model');
    menu_left = $("#menu_left");
}

// Init structure size adapted to screen
function initStucture() {
    windowHeight = $(window).height();
    windowWidth= $(window).width();

    content_right.width(windowWidth);
    content_right.height(windowHeight - 100);
    content.height(windowHeight - 105);
    content_admin_model.height(windowHeight-105-150);
    menu_left.height(windowHeight - 120);

    // View ressource links
    $("#ressources").height($("#content_admin_model").height());
}

// Slide toggle menu
function openMenu(elt) {

    var open = $(".elt_open");

    if(open.is(elt)) {
        elt.siblings("ul").slideUp(function() {
            elt.removeClass("elt_open");
        });
    } else {
        open.siblings("ul").slideUp(function() {
            open.removeClass();
        });
        elt.siblings("ul").slideDown();
        elt.addClass("elt_open");
        /*
        elt.siblings("ul").slideUp();
        elt.siblings("ul").removeClass("elt_open");
        */
    }
        

    /*
    $("#nav_model>ul>li").each(function() {
        if($(this) == elt) {
            console.log("this");
            $(this).siblings("ul").slideToggle();
        } else {
            console.log("test");
            $(this).siblings("ul").slideUp();
        }
     });
    */
}

