<?php
/* @var $this ModvideoController */
/* @var $model Modvideo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'source'); ?>
		<?php echo $form->textField($model,'source'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'embeded_code'); ?>
		<?php echo $form->textField($model,'embeded_code',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ref_project'); ?>
		<?php echo $form->textField($model,'ref_project'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'order_elt'); ?>
		<?php echo $form->textField($model,'order_elt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->