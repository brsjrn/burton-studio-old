<?php
/* @var $this ModsliderController */
/* @var $model Modslider */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'modslider-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row">
						<?php echo $form->labelEx($model,'ref_project'); ?>
						<?php echo $form->textField($model,'ref_project'); ?>
						<?php echo $form->error($model,'ref_project'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'order_elt'); ?>
						<?php echo $form->textField($model,'order_elt'); ?>
						<?php echo $form->error($model,'order_elt'); ?>
						<div class="clear"></div>
					</div>
						<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
