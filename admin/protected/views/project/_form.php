<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<div id="project-id" class="hidden_data"><?php echo $model->id ?></div>

		<div id="currentModuleId" class="hidden_data"></div>

					<div class="row">
						<?php echo $form->labelEx($model,'title'); ?>
						<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>63)); ?>
						<?php echo $form->error($model,'title'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'isStared'); ?>
						<?php echo $form->textField($model,'isStared'); ?>
						<?php echo $form->error($model,'isStared'); ?>
						<div class="clear"></div>
					</div>
									<div class="row">
					<?php echo $form->labelEx($model,'stared_description_text'); ?>
					<?php echo $form->textArea($model,'stared_description_text',array('id'=>'editorstared_description_text')); ?>
					<?php echo $form->error($model,'stared_description_text'); ?>
					<div class="clear"></div>
				</div>
								<div class="row">
						<?php echo $form->labelEx($model,'stared_order'); ?>
						<?php echo $form->textField($model,'stared_order'); ?>
						<?php echo $form->error($model,'stared_order'); ?>
						<div class="clear"></div>
					</div>
								<div class="row">
				<?php echo $form->labelEx($model,'stared_cover_image'); ?>
				<?php echo $form->fileField($model,'stared_cover_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'stared_cover_image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->stared_cover_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Project/' . $model->stared_cover_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
					<div class="row">
				<?php echo $form->labelEx($model,'cover_image'); ?>
				<?php echo $form->fileField($model,'cover_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'cover_image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->cover_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Project/' . $model->cover_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>

					<div class="row">
						<?php echo $form->labelEx($model,'location'); ?>
						<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>127)); ?>
						<?php echo $form->error($model,'location'); ?>
						<div class="clear"></div>
					</div>
					
					<div class="row">
						<?php echo $form->labelEx($model,'project_type'); ?>
						<?php echo $form->textField($model,'project_type'); ?>
						<?php echo $form->error($model,'project_type'); ?>
						<div class="clear"></div>
					</div>

					<div class="row">
						<?php echo $form->labelEx($model,'architect'); ?>
						<?php echo $form->textField($model,'architect'); ?>
						<?php echo $form->error($model,'architect'); ?>
						<div class="clear"></div>
					</div>

				<ul id="modules">

                    <?php
                        if(isset($listModTitles) && count($listModTitles) > 0) {
                            foreach ($listModTitles as $key => $modTitle) {
                                echo '
                                    <div class="module mod-title" id="mod-'. $modTitle->id .'" mod-id="'. $modTitle->id .'">
                                        <div class="module-header">
                                            <div class="module-type">Title</div>
                                            <div class="module-delete">delete</div>
                                            <div class="module-edit">edit</div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="module-content">
                                            <div class="module-content-inner">'. $modTitle->title .'</div>
                                        </div>
                                    </div>
                                ';
                            }
                        }
                    ?>

                    <?php
                        if(isset($listModTexts) && count($listModTitles) > 0) {
                            foreach ($listModTexts as $key => $modText) {
                                echo '
                                    <div class="module mod-text" id="mod-'. $modText->id .'" mod-id="'. $modText->id .'">
                                        <div class="module-header">
                                            <div class="module-type">Text</div>
                                            <div class="module-delete">delete</div>
                                            <div class="module-edit">edit</div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="module-content">
                                            <div class="module-content-inner">'. $modText->text .'</div>
                                        </div>
                                    </div>
                                ';
                            }
                        }
                    ?>

				</ul>

				<div class="row" id="add-module">

					<!-- Add Title -->
					<?php echo CHtml::link('Add title', "",  // the link for open the dialog
					    array(
                            'id'=>'btn-add-title',
                            'class'=>'btn-add-module',
					        'style'=>'cursor: pointer; text-decoration: underline;',
					        'onclick'=>"{addModTitle(); $('#dialogModTitle').dialog('open');}"));
			        ?>
					 
					<?php
					$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
					    'id'=>'dialogModTitle',
					    'options'=>array(
					        'title'=>'Add title',
					        'autoOpen'=>false,
					        'modal'=>true,
					        'width'=>550,
					        'height'=>470,
					    ),
					));?>
					<div class="divDialogFormTitle"></div>
					 
					<?php $this->endWidget();?>

					<!-- Add Text -->
					<?php echo CHtml::link('Add text', "",  // the link for open the dialog
					    array(
                            'id'=>'btn-add-text',
                            'class'=>'btn-add-module',
					        'style'=>'cursor: pointer; text-decoration: underline;',
					        'onclick'=>"{addModText(); $('#dialogModText').dialog('open');}"));
			        ?>
					 
					<?php
					$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
					    'id'=>'dialogModText',
					    'options'=>array(
					        'title'=>'Add text',
					        'autoOpen'=>false,
					        'modal'=>true,
					        'width'=>550,
					        'height'=>470,
					    ),
					));?>
					<div class="divDialogFormText"></div>
					 
					<?php $this->endWidget();?>

					<div class="clear"></div>
				</div>


				<div class="row">
					<?php echo $form->labelEx($model,'ordre'); ?>
					<?php 
						if($model->isNewRecord) {
	                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array(count($listOrders)=>array('selected'=>true))));
	                    } else {
	                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array($model->ordre=>array('selected'=>true))));
	                    }
					?>
					<?php echo $form->error($model,'ordre'); ?>
					<div class="clear"></div>
				</div>
				<div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	
	// Sortable modules
	$( function() {
	    $( "#modules" ).sortable();
	    $( "#modules" ).disableSelection();
	  } );
	
	CKEDITOR.replace( 'editorstared_description_text', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });

    /* TEXT MODULE */
    // -- Add
    function addModText()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('ModText/create'),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modtext_ref_project').val($('#project-id').html());
                        $('#Modtext_order_elt').val($('.module').length + 1);
                        $('#dialogModText div.divDialogFormText form').submit(addModText);
                    }
                    else
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        setTimeout(\"$('#dialogModText').dialog('close') \",1000);

                        $('#modules').append(createDivText(data.model.id, data.model.text));

                        //$('#modules').append(\"<li class='module mod-text' id='mod-\"+ data.model.id +\"'><div class='id'>\"+ data.model.id +\"</div><div class='title'>\"+ data.model.text +\"</div></li>\");
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Prepare udpate
    function prepareUpdateModText(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtext/update/'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'get',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        $('#Modtext_ref_project').val($('#project-id').html());
                        $('#dialogModText div.divDialogFormText form').submit(updateModText);
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Update
    function updateModText()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtext/update/0'),
                'data'=> "js:$(this).serialize() +'&id='+ $(\"#currentModuleId\").html()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        $('#ModText_ref_project').val($('#project-id').html());
                        $('#dialogModText div.divDialogFormText form').submit(updateModText);
                    }
                    else
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        setTimeout(\"$('#dialogModText').dialog('close') \",1000);

                        $('#modules').find('#mod-'+ data.model.id +'').find('.module-content-inner').html(data.model.text);
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Delete
    function deleteModText(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtext/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Open update
    $('#modules').on('click', '.mod-text .module-edit', function() {
    	var id = $(this).parents('.module').attr('mod-id');
    	console.log("ID : "+ id);

    	$('#currentModuleId').html(id);

    	prepareUpdateModText(id);
    	$('#dialogModText').dialog('open');
    });

    function createDivText(id, text) {
        return '<div class="module mod-text" id="mod-'+ id +'" mod-id="'+ id +'"><div class="module-header"><div class="module-type">Text</div><div class="module-delete">delete</div><div class="module-edit">edit</div><div class="clear"></div></div><div class="module-content"><div class="module-content-inner">'+ text +'</div></div></div>';
    }

    /* TITLE MODULE */
    // -- Add
    function addModTitle()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/create'),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modtitle_ref_project').val($('#project-id').html());
                        $('#Modtitle_order_elt').val($('.module').length + 1);
                        $('#dialogModTitle div.divDialogFormTitle form').submit(addModTitle);
                    }
                    else
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        setTimeout(\"$('#dialogModTitle').dialog('close') \",1000);

                        $('#modules').append(createDivTitle(data.model.id, data.model.title));

                        //$('#modules').append(\"<li class='module mod-title' id='mod-\"+ data.model.id +\"'><div class='id'>\"+ data.model.id +\"</div><div class='title'>\"+ data.model.title +\"</div></li>\");
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Prepare udpate
    function prepareUpdateModTitle(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/update/'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'get',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        $('#Modtitle_ref_project').val($('#project-id').html());
                        $('#dialogModTitle div.divDialogFormTitle form').submit(updateModTitle);
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Update
    function updateModTitle()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/update/0'),
                'data'=> "js:$(this).serialize() +'&id='+ $(\"#currentModuleId\").html()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        $('#Modtitle_ref_project').val($('#project-id').html());
                        $('#dialogModTitle div.divDialogFormTitle form').submit(updateModTitle);
                    }
                    else
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        setTimeout(\"$('#dialogModTitle').dialog('close') \",1000);

                        $('#modules').find('#mod-'+ data.model.id +'').find('.module-content-inner').html(data.model.title);
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Delete
    function deleteModTitle(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Open update
    $('#modules').on('click', '.mod-title .module-edit', function() {
    	var id = $(this).parents('.module').attr('mod-id');
    	console.log("ID : "+ id);

    	$('#currentModuleId').html(id);

    	prepareUpdateModTitle(id);
    	$('#dialogModTitle').dialog('open');
    });

    // Click to delete
    $('#modules').on('click', '.module-delete', function() {
        //.mod-title 
        var module = $(this).parents('.module');
        var id = module.attr('mod-id');
        console.log("ID delete : "+ id);

        $('#currentModuleId').html(id);

        if(module.hasClass('mod-title')) {
            console.log("Delete title");
            deleteModTitle(id);
        }
        if(module.hasClass('mod-text')) {
            console.log("Delete text");
            deleteModText(id);
        }
    });

    function createDivTitle(id, title) {
        return '<div class="module mod-title" id="mod-'+ id +'" mod-id="'+ id +'"><div class="module-header"><div class="module-type">Title</div><div class="module-delete">delete</div><div class="module-edit">edit</div><div class="clear"></div></div><div class="module-content"><div class="module-content-inner">'+ title +'</div></div></div>';
    }
			    
</script>
