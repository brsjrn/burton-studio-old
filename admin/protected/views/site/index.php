<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

if(Yii::app()->user->isGuest) {
    //$this->redirect(Yii::app()->getModule('user')->loginUrl);
    $this->redirect(array('site/login'));
}
?>

<div id="home_page">
    <div class="admin_info">Welcome on your administration page</div>
</div>