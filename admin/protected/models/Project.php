<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property integer $id
 * @property string $title
 * @property integer $isStared
 * @property string $stared_description_text
 * @property integer $stared_order
 * @property string $stared_cover_image
 * @property string $cover_image
 * @property string $location
 * @property string $project_type
 * @property string $architect
 * @property integer $ordre
 *
 * The followings are the available model relations:
 * @property Modslider[] $modsliders
 * @property Modtext[] $modtexts
 * @property Modtitle[] $modtitles
 * @property Modvideo[] $modvideos
 */
class Project extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isStared, stared_order, ordre', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>63),
			array('stared_cover_image, cover_image', 'length', 'max'=>255),
			array('location, project_type, architect', 'length', 'max'=>127),
			array('stared_description_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, isStared, stared_description_text, stared_order, stared_cover_image, cover_image, location, project_type, architect, ordre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'modsliders' => array(self::HAS_MANY, 'Modslider', 'ref_project'),
			'modtexts' => array(self::HAS_MANY, 'Modtext', 'ref_project'),
			'modtitles' => array(self::HAS_MANY, 'Modtitle', 'ref_project'),
			'modvideos' => array(self::HAS_MANY, 'Modvideo', 'ref_project'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'isStared' => 'Is Stared',
			'stared_description_text' => 'Stared Description Text',
			'stared_order' => 'Stared Order',
			'stared_cover_image' => 'Stared Cover Image',
			'cover_image' => 'Cover Image',
			'location' => 'Location',
			'project_type' => 'Project Type',
			'architect' => 'Architect',
			'ordre' => 'Ordre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('isStared',$this->isStared);
		$criteria->compare('stared_description_text',$this->stared_description_text,true);
		$criteria->compare('stared_order',$this->stared_order);
		$criteria->compare('stared_cover_image',$this->stared_cover_image,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('project_type',$this->project_type,true);
		$criteria->compare('architect',$this->architect,true);
		$criteria->compare('ordre',$this->ordre);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
