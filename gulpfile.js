// Requis
var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')()
	// Plugins
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	imagemin = require('gulp-imagemin'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	// Display messages (errors mainly)
	gutil = require('gulp-util'),
	// Manage errors to not kill pipe stream
	plumber = require('gulp-plumber'),
	// Notifications
	notify = require('gulp-notify');

// Variables de chemins
var source = './src'; // dossier de travail
var destination = './dist'; // dossier à livrer

// SASS
//Autoprefxer
gulp.task('css', function() {
	return gulp.src(source + '/assets/scss/style.scss')
		.pipe(
			plumber({errorHandler: notify.onError({
				message: "<%= error.message %>",
		        title: "CSS Error"

			})})
		)
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(plumber.stop())
		.pipe(gulp.dest(destination + '/assets/css/'))
});

// Javascript
gulp.task('javascript', function() {
  return gulp.src([source + '/assets/js/lib/*.js', source + '/assets/js/modules/*.js', source + '/assets/js/vendors/*.js', source + '/assets/js/*.js'])
    .pipe(concat('script.js'))
    //.pipe(uglify())
    .pipe(gulp.dest(destination + '/assets/js/'));
});

// Minify CSS
gulp.task('minify-css', function() {
	return gulp.src(destination + '/assets/css/style.css')
		.pipe(cleanCSS())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(destination + '/assets/css/'));
});

// Minify JS
gulp.task('minify-js', function() {
	return gulp.src(destination + '/assets/js/script.js')
		.pipe(uglify())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(destination + '/assets/js/'));
});


// Images
gulp.task('media', function() {
	return gulp.src(source + '/assets/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest(destination + '/assets/images/'));
});

// Build
gulp.task('build', ['css', 'javascript']);

// Prod
gulp.task('prod', ['media', 'css', 'javascript', 'minify-css', 'minify-js']);

// Watch
gulp.task('watch', function () {
	gulp.watch(source + '/assets/scss/**/*.scss', ['css']);
	gulp.watch(source + '/assets/js/**/*.js', ['javascript']);
});

// default
gulp.task('default', ['media', 'build', 'watch']);