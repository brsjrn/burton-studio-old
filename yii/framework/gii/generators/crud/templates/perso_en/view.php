<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('admin'),
	\$model->{$nameColumn},
);\n";
?>
?>

<div id="top_admin_model">
	<h1><?php echo $this->modelClass." #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?><span class="back_admin"><?php echo "<?php echo CHtml::link('back', array('$this->modelClass/admin')); ?>"; ?></span></h1>
	
	<div id="btn_model_area">
		<?php echo "<?php echo CHtml::link('', array('$this->modelClass/update', 'id'=>\$model->id), array('class'=>'btn_model glyphicon glyphicon-edit')); ?>"; ?>
		<?php echo "<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>\$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>"; ?>
		<?php echo "<?php echo CHtml::link('', array('$this->modelClass/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>"; ?>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<?php 
foreach($this->tableSchema->columns as $column) {
	echo '<div class="view_attribute">';
		echo '<div class="view_attribute_name">'. $column->name .'</div>';
		if(strpos($column->name, 'image') !== FALSE ) {
			echo '<div class="view_attribute_value view_attribute_image"><?php echo isset($model->'. $column->name .') ? CHtml::image("../../../images/'. $this->modelClass .'/". $model->'. $column->name .',"image",array("class"=>"image_preview")):"-"; ?></div>';
		} else if(strpos($column->name, 'text') !== FALSE ) {
			echo '<div class="view_attribute_value view_attribute_text"><?php echo isset($model->'. $column->name .') ? $model->'. $column->name .' : "-"; ?></div>';
		} else {
			echo '<div class="view_attribute_value"><?php echo isset($model->'. $column->name .') ? $model->'. $column->name .' : "-"; ?></div>';
		}
		
		echo '<div class="clear"></div>';
    echo '</div>';
}
?>
</div>