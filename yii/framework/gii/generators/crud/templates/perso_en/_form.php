<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */

	<?php echo "
		\$baseUrl = Yii::app()->baseUrl; 
		\$cs = Yii::app()->getClientScript();
		\$cs->registerScriptFile(\$baseUrl.'/js/form_script.js');
	"; ?>
?>

<?php
	echo "<script src=\"<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>\"></script>";
?>

<div class="form">

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>\n"; ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<?php
foreach($this->tableSchema->columns as $key=>$column)
{
	if($column->autoIncrement)
		continue;

	// IMAGE
	if(strpos($column->name, 'image') !== FALSE ) {
    	?>
			<div class="row">
				<?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
				<?php echo "<?php echo \$form->fileField(\$model,'$column->name',array('size'=>60,'maxlength'=>255)); ?>\n"; ?>
				<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
				<div class="clear"></div>
				<?php
					echo "
						<?php if(!\$model->isNewRecord && \$model->$column->name != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/$this->modelClass/' . \$model->$column->name,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        ";
				?>
			</div>
		<?php
	} else {

		// TEXT
		if(strpos($column->name, 'text') !== FALSE ) {
			?>
				<div class="row">
					<?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
					<?php echo "<?php echo \$form->textArea(\$model,'$column->name',array('id'=>'editor". $key ."')); ?>\n"; ?>
					<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
					<div class="clear"></div>
				</div>
			<?php
		} else {
			
			// ORDER
			if($column->name == 'ordre' ) {
				?>
					<div class="row">
						<?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
						<?php echo "<?php 
										if(\$model->isNewRecord) {
					                        echo \$form->dropDownList(\$model, '$column->name', \$listOrders, array('options'=>array(count(\$listOrders)=>array('selected'=>true))));
					                    } else {
					                        echo \$form->dropDownList(\$model, '$column->name', \$listOrders, array('options'=>array(\$model->$column->name=>array('selected'=>true))));
					                    }
									?>";
				                    
				                ?>
						<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
						<div class="clear"></div>
					</div>
				<?php
			} else {
				if(strpos($column->name, 'date') !== FALSE ) {
				?>
					<div class="row">
						<?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
						<?php echo "<?php 
										\$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					                        'model' => \$model,
					                        'attribute' => '$column->name',
					                        'language' => 'en',
					                        'options' => array(
					                            'showOn' => 'both', 
					                            'dateFormat' => 'yy-mm-dd',
					                        ),
					                        'htmlOptions' => array(
					                            'size' => '10',         // textField size
					                            'maxlength' => '10',    // textField maxlength
					                        ),
					                    ));
									?>";
				                    
				                ?>
						<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
						<div class="clear"></div>
					</div>
				<?php
				} else {
				?>
					<div class="row">
						<?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
						<?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
						<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
						<div class="clear"></div>
					</div>
					<?php
				}
			}
		}
	}
}
?>
	<div class="row buttons">
		<?php echo "<?php echo CHtml::submitButton(\$model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>\n"; ?>
	</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div><!-- form -->


<script type='text/javascript'>

	<?php
	foreach($this->tableSchema->columns as $key=>$column) {
		if(strpos($column->name, 'text') !== FALSE ) {
			echo "
				CKEDITOR.replace( 'editor". $key ."', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			";
		}
	}
	?>
    
</script>
