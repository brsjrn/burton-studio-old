# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# H�te: 127.0.0.1 (MySQL 5.5.38)
# Base de donn�es: burton_studio
# Temps de g�n�ration: 2017-02-23 13:42:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table modText
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modText`;

CREATE TABLE `modText` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `text` text,
  `ref_project` int(11) DEFAULT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Affichage de la table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `isStared` int(11) DEFAULT NULL,
  `stared_description_text` text,
  `stared_cover_image` varchar(255) DEFAULT NULL,
  `stared_order` int(11) DEFAULT NULL,
  `cover_image` varchar(255) DEFAULT NULL,
  `location` varchar(63) DEFAULT NULL,
  `project_type` varchar(255) DEFAULT NULL,
  `description_text` text,
  `quote` text,
  `quote_signature_name` varchar(63) DEFAULT NULL,
  `quote_signature_role` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
