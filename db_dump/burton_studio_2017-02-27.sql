# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# H�te: 127.0.0.1 (MySQL 5.5.38)
# Base de donn�es: burton_studio
# Temps de g�n�ration: 2017-02-27 14:51:24 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table modText
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modText`;

CREATE TABLE `modText` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  KEY `ref_project_2` (`ref_project`),
  CONSTRAINT `modText_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `modText` WRITE;
/*!40000 ALTER TABLE `modText` DISABLE KEYS */;

INSERT INTO `modText` (`id`, `title`, `text`, `ref_project`, `order_elt`)
VALUES
	(5,'test text','qsdqsdqsd',1,1);

/*!40000 ALTER TABLE `modText` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table modVideo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modVideo`;

CREATE TABLE `modVideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) DEFAULT NULL,
  `embeded_code` varchar(255) DEFAULT NULL,
  `ref_project` int(11) NOT NULL,
  `order_elt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_project` (`ref_project`),
  CONSTRAINT `modVideo_ibfk_1` FOREIGN KEY (`ref_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Affichage de la table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) DEFAULT NULL,
  `isStared` int(11) DEFAULT NULL,
  `stared_description_text` text,
  `stared_order` int(11) DEFAULT NULL,
  `stared_cover_image` varchar(255) DEFAULT NULL,
  `cover_image` varchar(255) DEFAULT NULL,
  `location` varchar(127) DEFAULT NULL,
  `poject_type` int(11) DEFAULT NULL,
  `description_text` text,
  `quote_text` text,
  `signature_name` varchar(127) DEFAULT NULL,
  `signature_role` varchar(127) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `title`, `isStared`, `stared_description_text`, `stared_order`, `stared_cover_image`, `cover_image`, `location`, `poject_type`, `description_text`, `quote_text`, `signature_name`, `signature_role`, `ordre`)
VALUES
	(1,'project 1',NULL,'',NULL,'','','',NULL,'','','','',1);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
