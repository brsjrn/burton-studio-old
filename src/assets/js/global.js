'use strict';

// Var global
var win;

// Var sizes
var large_width,
    ratio_image;

$(document).ready(function(){
    // Init
    initVariables();
    resizeBrowser();

    // Resize window
    $(window).resize(function() {
        resizeBrowser();
    });

    // Scroll manager
    win.on("scroll", function() {

    });

    // Call functions
    new $.defilement();
});

function initVariables() {
    win = $(window);
    large_width = $('.large-width');
    ratio_image = $('.ratio-image')
}

function resizeBrowser() {
    var marge = 100,
        video_width_ratio = 2;

    // Resize dynamic responsive
    if(viewport().width <= 1080) {

    } else {

    }

    // Resize full-images
    $('.full-image').each(function() {
        var parent = $(this).parent('.full-image-parent');
        adaptImageToResolution(parent, $(this))
    });

}

/* ----------------------- */
/*          TOOLS          */
/* ----------------------- */
// Get viewport dimensions
function viewport() {
    var e = window, a = 'inner';
    if(!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ]};
}

/* adapte project detail cover to resolution */
function adaptImageToResolution(ref, target) {
    var newX;
    var newY;
    var deltaX;
    var deltaY;
    var deltaMax;
    var moveX;
    var moveY;

    var refWidth = ref.width();
    var targetWidth = target.width();
    deltaX = refWidth/targetWidth;
    deltaY = ref.height()/target.height();

    deltaMax = Math.max(deltaX, deltaY);

    newX = target.width()*deltaMax;
    newY = target.height()*deltaMax;

    moveX = (newX - ref.width())/2;
    moveY = (newY - ref.height())/2;

    target.width(newX);
    target.height(newY);
    target.css("margin-left", '-'+moveX+'px');
    target.css("margin-top", '-'+moveY+'px');
}