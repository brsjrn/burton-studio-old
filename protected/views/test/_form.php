<?php
/* @var $this TestController */
/* @var $model Test */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'test-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row">
						<?php echo $form->labelEx($model,'title'); ?>
						<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>63)); ?>
						<?php echo $form->error($model,'title'); ?>
						<div class="clear"></div>
					</div>
			<div class="row">
				<?php echo $form->labelEx($model,'image'); ?>
				<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'image'); ?>
				<div class="clear"></div>
				
				<?php if(!$model->isNewRecord && $model->image != ''){ ?>
			        <div class='image_already_exist'>
			             <?php echo CHtml::image('../../../../images/Test/' . $model->image,'image',array('class'=>'image_preview')); ?>
			        </div>
		        <?php } ?>
			</div>
			<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
