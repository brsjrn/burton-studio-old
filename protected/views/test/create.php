<?php
/* @var $this TestController */
/* @var $model Test */

$this->breadcrumbs=array(
	'Tests'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Test', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Create Test<span class="back_admin"><?php echo CHtml::link('back', array('Test/admin')); ?></span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?></div>