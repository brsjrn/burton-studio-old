<?php
/* @var $this ModtextController */
/* @var $model Modtext */

$this->breadcrumbs=array(
	'Modtexts'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Modtext', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#modtext-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Modtexts</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Modtext/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'modtext-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'text',
		'ref_project',
		'order_elt',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>