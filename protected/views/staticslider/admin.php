<?php
/* @var $this StaticsliderController */
/* @var $model Staticslider */

$this->breadcrumbs=array(
	'Staticsliders'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Staticslider', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#staticslider-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Staticsliders</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Staticslider/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'staticslider-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'getTitle',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>