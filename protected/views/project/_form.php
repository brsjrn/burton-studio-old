<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<div id="project-id" class="hidden_data"><?php echo $model->id ?></div>

		<div id="currentModuleId" class="hidden_data"></div>

				<div class="row">
					<?php echo $form->labelEx($model,'title'); ?>
					<?php echo $form->textField($model,'title'); ?>
					<?php echo $form->error($model,'title'); ?>
					<div class="clear"></div>
				</div>

				<div class="row">
					<?php echo $form->labelEx($model,'isStared'); ?>
					<?php echo $form->checkBox($model,'isStared', array('class' => 'checkHasTarget')); ?>
					<?php echo $form->error($model,'isStared'); ?>
					<div class="clear"></div>
				</div>

                <div class="hidden-area" id="Project_isStared_target">
    				<div class="row">
    					<?php echo $form->labelEx($model,'stared_description_text'); ?>
    					<?php echo $form->textArea($model,'stared_description_text',array('rows'=>6, 'cols'=>50, 'class' => 'hidden-elt')); ?>
    					<?php echo $form->error($model,'stared_description_text'); ?>
    					<div class="clear"></div>
    				</div>

    				<div class="row">
    					<?php echo $form->labelEx($model,'stared_order'); ?>
    					<?php echo $form->textField($model,'stared_order', array('class' => 'hidden-elt')); ?>
    					<?php echo $form->error($model,'stared_order'); ?>
    					<div class="clear"></div>
    				</div>

    				<div class="row">
        				<?php echo $form->labelEx($model,'stared_cover_image'); ?>
        				<?php echo $form->fileField($model,'stared_cover_image',array('size'=>60,'maxlength'=>255)); ?>
        				<?php echo $form->error($model,'stared_cover_image'); ?>
    				    <div class="clear"></div>
    				
    					<?php if(!$model->isNewRecord && $model->stared_cover_image != ''){ ?>
    				        <div class='image_already_exist hidden-image-exist'>
    				             <?php echo CHtml::image('../../../images/Project/' . $model->stared_cover_image,'image',array('class'=>'image_preview')); ?>
    				        </div>
    			        <?php } ?>
    		        </div>
                </div>

				<div class="row">
    				<?php echo $form->labelEx($model,'cover_image'); ?>
    				<?php echo $form->fileField($model,'cover_image',array('size'=>60,'maxlength'=>255)); ?>
    				<?php echo $form->error($model,'cover_image'); ?>
    				<div class="clear"></div>
				
					<?php if(!$model->isNewRecord && $model->cover_image != ''){ ?>
				        <div class='image_already_exist'>
				             <?php echo CHtml::image('../../../images/Project/' . $model->cover_image,'image',array('class'=>'image_preview')); ?>
				        </div>
			        <?php } ?>
		        </div>

				<div class="row">
					<?php echo $form->labelEx($model,'location'); ?>
					<?php echo $form->textField($model,'location'); ?>
					<?php echo $form->error($model,'location'); ?>
					<div class="clear"></div>
				</div>
				
				<div class="row">
					<?php echo $form->labelEx($model,'project_type'); ?>
					<?php echo $form->textField($model,'project_type'); ?>
					<?php echo $form->error($model,'project_type'); ?>
					<div class="clear"></div>
				</div>

				<div class="row">
					<?php echo $form->labelEx($model,'architect'); ?>
					<?php echo $form->textField($model,'architect'); ?>
					<?php echo $form->error($model,'architect'); ?>
					<div class="clear"></div>
				</div>

                <div class="row">
                    <?php echo $form->labelEx($model,'ordre'); ?>
                    <?php 
                        if($model->isNewRecord) {
                            echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array(count($listOrders)=>array('selected'=>true))));
                        } else {
                            echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array($model->ordre=>array('selected'=>true))));
                        }
                    ?>
                    <?php echo $form->error($model,'ordre'); ?>
                    <div class="clear"></div>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'description_seo'); ?>
                    <?php echo $form->textField($model,'description_seo', array('size'=>60,'maxlength'=>140)); ?>
                    <?php echo $form->error($model,'description_seo'); ?>
                    <div class="clear"></div>
                </div>

				<ul id="modules">

                    <?php
                        if(isset($orderedModules)) {

                            foreach ($orderedModules as $key => $module) {
                                $className = get_class($module);

                                switch ($className) {
                                    case 'Modtitle':
                                        echo '
                                            <div class="module mod-title" id="mod-title-'. $module->id .'" mod-id="'. $module->id .'">
                                                <div class="module-header">
                                                    <div class="module-type">Title</div>
                                                    <div class="module-delete">delete</div>
                                                    <div class="module-edit">edit</div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="module-content">
                                                    <div class="module-content-inner">'. $module->title .'</div>
                                                </div>
                                            </div>
                                        ';
                                        break;
                                    case 'Modtext':
                                        echo '
                                            <div class="module mod-text" id="mod-text-'. $module->id .'" mod-id="'. $module->id .'">
                                                <div class="module-header">
                                                    <div class="module-type">Text</div>
                                                    <div class="module-delete">delete</div>
                                                    <div class="module-edit">edit</div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="module-content">
                                                    <div class="module-content-inner">'. $module->text .'</div>
                                                </div>
                                            </div>
                                        ';
                                        break;
                                    case 'Modvideo':
                                        echo '
                                            <div class="module mod-video" id="mod-video-'. $module->id .'" mod-id="'. $module->id .'">
                                                <div class="module-header">
                                                    <div class="module-type">Video</div>
                                                    <div class="module-delete">delete</div>
                                                    <div class="module-edit">edit</div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="module-content">
                                                    <div class="module-content-inner">
                                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/'. $module->embeded_code .'" frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="module-content-inner">
                                                         <img src="'. $baseUrl .'/images/Modvideo/'.$module->image_cover .'" class="preview">
                                                    </div>
                                                </div>
                                            </div>
                                        ';
                                        break;
                                    case 'Modimage':
                                        echo '
                                            <div class="module mod-image" id="mod-image-'. $module->id .'" mod-id="'. $module->id .'">
                                                <div class="module-header">
                                                    <div class="module-type">Freestyle</div>
                                                    <div class="module-delete">delete</div>
                                                    <div class="module-edit">edit</div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="module-content">
                                                    <img src="'. $baseUrl .'/images/Modimage/'.$module->image .'" class="preview">
                                                </div>
                                            </div>
                                        ';
                                        break;
                                    case 'Modslider':
                                        echo '
                                            <div class="module mod-slider" id="mod-slider-'. $module->id .'" mod-id="'. $module->id .'">
                                                <div class="module-header">
                                                    <div class="module-type">Slider</div>
                                                    <div class="module-delete">delete</div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="module-content">
                                                    <a href="'. CController::createUrl('Sliderimage/adminById', array('refModelId'=>$module->id)) .'" class="manage-images-slider">Manage slider images (<b>remember to save project before</b>)</a>
                                                </div>
                                            </div>
                                        ';
                                        break;
                                    default:
                                        # code...
                                        break;
                                }
                            }
                        }
                    ?>

				</ul>

				<div class="row" id="add-module">

                    <div class="admin-info">Please save before you <b>manage sliders images</b>, otherwise your last inputs will be lost.</div>

					<!-- Add Title -->
					<?php echo CHtml::link('Add title', "",  // the link for open the dialog
					    array(
                            'id'=>'btn-add-title',
                            'class'=>'btn-add-module',
					        'style'=>'cursor: pointer; text-decoration: underline;',
					        'onclick'=>"{addModTitle(); $('#dialogModTitle').dialog('open');}"));
			        ?>
					 
					<?php
					$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
					    'id'=>'dialogModTitle',
					    'options'=>array(
					        'title'=>'Add title',
					        'autoOpen'=>false,
					        'modal'=>true,
					        'width'=>550,
					        'height'=>470,
					    ),
					));?>
					<div class="divDialogFormTitle"></div>
					<?php $this->endWidget();?>

					<!-- Add Text -->
					<?php echo CHtml::link('Add text', "",  // the link for open the dialog
					    array(
                            'id'=>'btn-add-text',
                            'class'=>'btn-add-module',
					        'style'=>'cursor: pointer; text-decoration: underline;',
					        'onclick'=>"{addModText(); $('#dialogModText').dialog('open');}"));
			        ?>
					 
					<?php
					$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
					    'id'=>'dialogModText',
					    'options'=>array(
					        'title'=>'Add text',
					        'autoOpen'=>false,
					        'modal'=>true,
					        'width'=>550,
					        'height'=>470,
					    ),
					));?>
					<div class="divDialogFormText"></div>
					<?php $this->endWidget();?>

                    <!-- Add Video -->
                    <?php echo CHtml::link('Add video', "",  // the link for open the dialog
                        array(
                            'id'=>'btn-add-video',
                            'class'=>'btn-add-module',
                            'style'=>'cursor: pointer; text-decoration: underline;',
                            'onclick'=>"{addModVideo(); $('#dialogModVideo').dialog('open');}"));
                    ?>
                     
                    <?php
                    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
                        'id'=>'dialogModVideo',
                        'options'=>array(
                            'title'=>'Add video',
                            'autoOpen'=>false,
                            'modal'=>true,
                            'width'=>550,
                            'height'=>470,
                        ),
                    ));?>
                    <div class="divDialogFormVideo"></div>
                    <?php $this->endWidget();?>

                    <!-- Add Image -->
                    <?php echo CHtml::link('Add freestyle', "",  // the link for open the dialog
                        array(
                            'id'=>'btn-add-image',
                            'class'=>'btn-add-module',
                            'style'=>'cursor: pointer; text-decoration: underline;',
                            'onclick'=>"{addModImage(); $('#dialogModImage').dialog('open');}"));
                    ?>
                     
                    <?php
                    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
                        'id'=>'dialogModImage',
                        'options'=>array(
                            'title'=>'Add freestyle',
                            'autoOpen'=>false,
                            'modal'=>true,
                            'width'=>550,
                            'height'=>470,
                        ),
                    ));?>
                    <div class="divDialogFormImage"></div>
                    <?php $this->endWidget();?>

                    <!-- Add Slider -->
                    <?php echo CHtml::link('Add slider', "",  // the link for open the dialog
                        array(
                            'id'=>'btn-add-slider',
                            'class'=>'btn-add-module',
                            'style'=>'cursor: pointer; text-decoration: underline;',
                            'onclick'=>"{addModSlider(); $('#dialogModSlider').dialog('open');}"));
                    ?>
                     
                    <?php
                    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
                        'id'=>'dialogModSlider',
                        'options'=>array(
                            'title'=>'Add slider',
                            'autoOpen'=>false,
                            'modal'=>true,
                            'width'=>550,
                            'height'=>470,
                        ),
                    ));?>
                    <div class="divDialogFormSlider"></div>
                    <?php $this->endWidget();?>

					<div class="clear"></div>
				</div>
				<div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	
	// Sortable modules
	$( function() {
	    $( "#modules" ).sortable({
            stop: function (event, ui) {
                updateModuleOrders();
            },
            distance: 15
        });
	    $( "#modules" ).disableSelection();
	  } );

    function updateModuleOrders() {
        var cpt = 1;
        $('.module').each(function(i, val) {
            var splited = $(this).attr('id').split("-"),
                modType = splited[1],
                modId = splited[2];

            switch(modType) {
                case 'title':
                    console.log("Title");
                    $.ajax( 
                        {
                            url: '<?php echo Yii::app()->createUrl("Modtitle/updateOrder/"); ?>/'+ modId,
                            type: 'POST',
                            data: {order: cpt},
                            dataType: 'json',
                            success: function(data)
                            {
                                if(data.status == "success") {
                                    console.log('Update title : '+ modId +' => '+ data.newOrder);
                                } else {
                                    console.log('Fail to update title');
                                }
                            }
                        }
                    );

                    break;
                case 'image':
                    console.log("Image");
                    $.ajax( 
                        {
                            url: '<?php echo Yii::app()->createUrl("Modimage/updateOrder/"); ?>/'+ modId,
                            type: 'POST',
                            data: {order: cpt},
                            dataType: 'json',
                            success: function(data)
                            {
                                if(data.status == "success") {
                                    console.log('Update image : '+ modId +' => '+ data.newOrder);
                                } else {
                                    console.log('Fail to update image');
                                }
                            }
                        }
                    );

                    break;
                case 'video':
                    console.log("Video");
                    $.ajax( 
                        {
                            url: '<?php echo Yii::app()->createUrl("Modvideo/updateOrder/"); ?>/'+ modId,
                            type: 'POST',
                            data: {order: cpt},
                            dataType: 'json',
                            success: function(data)
                            {
                                if(data.status == "success") {
                                    console.log('Update video : '+ modId +' => '+ data.newOrder);
                                } else {
                                    console.log('Fail to update video');
                                }
                            }
                        }
                    );

                    break;
                case 'text':
                    console.log("Text");
                    $.ajax( 
                        {
                            url: '<?php echo Yii::app()->createUrl("Modtext/updateOrder/"); ?>/'+ modId,
                            type: 'POST',
                            data: {order: cpt},
                            dataType: 'json',
                            success: function(data)
                            {
                                if(data.status == "success") {
                                    console.log('Update text : '+ modId +' => '+ data.newOrder);
                                } else {
                                    console.log('Fail to update text');
                                }
                            }
                        }
                    );

                    break;
                case 'slider':
                    console.log("Slider");
                    $.ajax( 
                        {
                            url: '<?php echo Yii::app()->createUrl("Modslider/updateOrder/"); ?>/'+ modId,
                            type: 'POST',
                            data: {order: cpt},
                            dataType: 'json',
                            success: function(data)
                            {
                                if(data.status == "success") {
                                    console.log('Update slider : '+ modId +' => '+ data.newOrder);
                                } else {
                                    console.log('Fail to update slider');
                                }
                            }
                        }
                    );

                    break;
                default:
                    
            }

            cpt++;
        });
    }
	
    /*
	CKEDITOR.replace( 'editorstared_description_text', {
         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
    */

    /* TEXT MODULE */
    // -- Add
    function addModText()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtext/create'),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modtext_ref_project').val($('#project-id').html());
                        $('#Modtext_order_elt').val($('.module').length + 1);
                        $('#dialogModText div.divDialogFormText form').submit(addModText);
                    }
                    else
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        setTimeout(\"$('#dialogModText').dialog('close') \",1000);

                        $('#modules').append(createDivText(data.model.id, data.model.text));

                        //$('#modules').append(\"<li class='module mod-text' id='mod-\"+ data.model.id +\"'><div class='id'>\"+ data.model.id +\"</div><div class='title'>\"+ data.model.text +\"</div></li>\");
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Prepare udpate
    function prepareUpdateModText(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtext/update/'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'get',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        $('#Modtext_ref_project').val($('#project-id').html());
                        $('#dialogModText div.divDialogFormText form').submit(updateModText);
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Update
    function updateModText()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtext/update/0'),
                'data'=> "js:$(this).serialize() +'&id='+ $(\"#currentModuleId\").html()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        $('#ModText_ref_project').val($('#project-id').html());
                        $('#dialogModText div.divDialogFormText form').submit(updateModText);
                    }
                    else
                    {
                        $('#dialogModText div.divDialogFormText').html(data.div);
                        setTimeout(\"$('#dialogModText').dialog('close') \",1000);

                        $('#modules').find('#mod-text-'+ data.model.id +'').find('.module-content-inner').html(data.model.text);
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Delete
    function deleteModText(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtext/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-text-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Open update
    $('#modules').on('click', '.mod-text .module-edit', function() {
    	var id = $(this).parents('.module').attr('mod-id');
    	console.log("ID : "+ id);

    	$('#currentModuleId').html(id);

    	prepareUpdateModText(id);
    	$('#dialogModText').dialog('open');
    });

    function createDivText(id, text) {
        return '<div class="module mod-text" id="mod-text-'+ id +'" mod-id="'+ id +'"><div class="module-header"><div class="module-type">Text</div><div class="module-delete">delete</div><div class="module-edit">edit</div><div class="clear"></div></div><div class="module-content"><div class="module-content-inner">'+ text +'</div></div></div>';
    }

    /* TITLE MODULE */
    // -- Add
    function addModTitle()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/create'),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modtitle_ref_project').val($('#project-id').html());
                        $('#Modtitle_order_elt').val($('.module').length + 1);
                        $('#dialogModTitle div.divDialogFormTitle form').submit(addModTitle);
                    }
                    else
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        setTimeout(\"$('#dialogModTitle').dialog('close') \",1000);

                        $('#modules').append(createDivTitle(data.model.id, data.model.title));

                        //$('#modules').append(\"<li class='module mod-title' id='mod-\"+ data.model.id +\"'><div class='id'>\"+ data.model.id +\"</div><div class='title'>\"+ data.model.title +\"</div></li>\");
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Prepare udpate
    function prepareUpdateModTitle(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/update/'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'get',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        $('#Modtitle_ref_project').val($('#project-id').html());
                        $('#dialogModTitle div.divDialogFormTitle form').submit(updateModTitle);
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Update
    function updateModTitle()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/update/0'),
                'data'=> "js:$(this).serialize() +'&id='+ $(\"#currentModuleId\").html()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        $('#Modtitle_ref_project').val($('#project-id').html());
                        $('#dialogModTitle div.divDialogFormTitle form').submit(updateModTitle);
                    }
                    else
                    {
                        $('#dialogModTitle div.divDialogFormTitle').html(data.div);
                        setTimeout(\"$('#dialogModTitle').dialog('close') \",1000);

                        $('#modules').find('#mod-title-'+ data.model.id +'').find('.module-content-inner').html(data.model.title);
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Delete
    function deleteModTitle(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modtitle/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-title-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Open update
    $('#modules').on('click', '.mod-title .module-edit', function() {
    	var id = $(this).parents('.module').attr('mod-id');
    	console.log("ID : "+ id);

    	$('#currentModuleId').html(id);

    	prepareUpdateModTitle(id);
    	$('#dialogModTitle').dialog('open');
    });

    function createDivTitle(id, title) {
        return '<div class="module mod-title" id="mod-title-'+ id +'" mod-id="'+ id +'"><div class="module-header"><div class="module-type">Title</div><div class="module-delete">delete</div><div class="module-edit">edit</div><div class="clear"></div></div><div class="module-content"><div class="module-content-inner">'+ title +'</div></div></div>';
    }

    /* VIDEO MODULE */
    // -- Add
    /*
    function addModVideo()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modvideo/create'),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modvideo_ref_project').val($('#project-id').html());
                        $('#Modvideo_order_elt').val($('.module').length + 1);
                        $('#dialogModVideo div.divDialogFormVideo form').submit(addModVideo);
                    }
                    else
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        setTimeout(\"$('#dialogModVideo').dialog('close') \",1000);

                        $('#modules').append(createDivVideo(data.model.id, data.model.embeded_code));

                        //$('#modules').append(\"<li class='module mod-video' id='mod-\"+ data.model.id +\"'><div class='id'>\"+ data.model.id +\"</div><div class='title'>\"+ data.model.embeded_code +\"</div></li>\");
                    }
     
                } ",
                ))?>;
        return false; 
    }
    */

    // -- Prepare udpate
    /*
    function prepareUpdateModVideo(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modvideo/update/'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'get',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        $('#Modvideo_ref_project').val($('#project-id').html());
                        $('#dialogModVideo div.divDialogFormVideo form').submit(updateModVideo);
                    }
     
                } ",
                ))?>;
        return false; 
     
    }
    */

    // -- Update
    /*
    function updateModVideo()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modvideo/update/0'),
                'data'=> "js:$(this).serialize() +'&id='+ $(\"#currentModuleId\").html()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        $('#Modvideo_ref_project').val($('#project-id').html());
                        $('#dialogModVideo div.divDialogFormVideo form').submit(updateModVideo);
                    }
                    else
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        setTimeout(\"$('#dialogModVideo').dialog('close') \",1000);

                        $('#modules').find('#mod-video-'+ data.model.id +'').find('.module-content-inner').html(data.model.embeded_code);
                    }
     
                } ",
                ))?>;
        return false; 
    }
    */

    // -- Delete
    /*
    function deleteModVideo(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modvideo/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-video-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }
    */

    function addModVideo()
    {
        console.log("Create mode video");

        var formData = new FormData($("#modvideo-form")[0])

        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Modvideo/create"); ?>',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        console.log("failure");
                        //$('#dialogImage div.divDialogForm form').submit(updateImage(id));

                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modvideo_ref_project').val($('#project-id').html());
                        $('#Modvideo_order_elt').val($('.module').length + 1);
                        //$('#dialogModImage div.divDialogFormImage form').submit(addModImage);
                    }
                }
            }
        );

        return false; 
    }

    function prepareUpdateModVideo(id)
    {
        console.log("Prepare for id : "+ id);
        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Modvideo/update/"); ?>/'+ id,
                type: 'POST',
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        $('#dialogModVideo div.divDialogFormVideo form').submit(updateModVideo(id));
                    }
                }
            }
        );

        return false; 
    }

    // -- Update
    function updateModVideo(id)
    {
        console.log("Update id : "+ id);

        var formData = new FormData($("#modvideo-form")[0])

        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Modvideo/update/"); ?>/'+ id,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        console.log("failure");
                        //$('#dialogImage div.divDialogForm form').submit(updateImage(id));
                    }
                    else
                    {
                        console.log("succes");
                        $('#dialogModVideo div.divDialogFormVideo').html(data.div);
                        setTimeout(function() {
                            $('#dialogModVideo').dialog('close')
                        }, 1000);
                        $('.video-'+ data.model.id +'').html(data.model.value);
                    }

                }
            }
        );

        return false; 
    }

    // -- Delete
    function deleteModVideo(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modvideo/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-video-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Open update
    $('#modules').on('click', '.mod-video .module-edit', function() {
        var id = $(this).parents('.module').attr('mod-id');
        console.log("ID : "+ id);

        $('#currentModuleId').html(id);

        prepareUpdateModVideo(id);
        $('#dialogModVideo').dialog('open');
    });

    function createDivVideo(id, code) {
        return '<div class="module mod-video" id="mod-video-'+ id +'" mod-id="'+ id +'"><div class="module-header"><div class="module-type">Video</div><div class="module-delete">delete</div><div class="module-edit">edit</div><div class="clear"></div></div><div class="module-content"><div class="module-content-inner">'+ code +'</div></div></div>';
    }

    /* SLIDER MODULE */
    // -- Add
    function addModSlider()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modslider/create'),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModSlider div.divDialogFormSlider').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modslider_ref_project').val($('#project-id').html());
                        $('#Modslider_order_elt').val($('.module').length + 1);
                        $('#dialogModSlider div.divDialogFormSlider form').submit(addModSlider);
                    }
                    else
                    {
                        $('#dialogModSlider div.divDialogFormSlider').html(data.div);
                        setTimeout(\"$('#dialogModSlider').dialog('close') \",1000);

                        $('#modules').append(createDivSlider(data.model.id, data.model.embeded_code));

                        //$('#modules').append(\"<li class='module mod-slider' id='mod-\"+ data.model.id +\"'><div class='id'>\"+ data.model.id +\"</div><div class='title'>\"+ data.model.embeded_code +\"</div></li>\");
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Prepare udpate
    function prepareUpdateModSlider(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modslider/update/'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'get',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModSlider div.divDialogFormSlider').html(data.div);
                        $('#Modslider_ref_project').val($('#project-id').html());
                        $('#dialogModSlider div.divDialogFormSlider form').submit(updateModSlider);
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Update
    function updateModSlider()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modslider/update/0'),
                'data'=> "js:$(this).serialize() +'&id='+ $(\"#currentModuleId\").html()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModSlider div.divDialogFormSlider').html(data.div);
                        $('#Modslider_ref_project').val($('#project-id').html());
                        $('#dialogModSlider div.divDialogFormSlider form').submit(updateModSlider);
                    }
                    else
                    {
                        $('#dialogModSlider div.divDialogFormSlider').html(data.div);
                        setTimeout(\"$('#dialogModSlider').dialog('close') \",1000);

                        $('#modules').find('#mod-slider-'+ data.model.id +'').find('.module-content-inner').html(data.model.embeded_code);
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Delete
    function deleteModSlider(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modslider/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-slider-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Open update
    $('#modules').on('click', '.mod-slider .module-edit', function() {
        var id = $(this).parents('.module').attr('mod-id');
        console.log("ID : "+ id);

        $('#currentModuleId').html(id);

        prepareUpdateModSlider(id);
        $('#dialogModSlider').dialog('open');
    });

    function createDivSlider(id, code) {
        return '<div class="module mod-slider" id="mod-slider-'+ id +'" mod-id="'+ id +'"><div class="module-header"><div class="module-type">Slider</div><div class="module-delete">delete</div><div class="clear"></div></div><div class="module-content"><a href="<?php echo CController::createUrl('Sliderimage/adminById?refModelId=') ?>'+ id +'" class="link-manage-image">Manage slider images (<b>remember to save project before</b>)</a></div></div>';
    }

    /* IMAGE MODULE */
    // -- Add
    /*
    function addModImage()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('ModImage/create'),
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modimage_ref_project').val($('#project-id').html());
                        $('#Modimage_order_elt').val($('.module').length + 1);
                        $('#dialogModImage div.divDialogFormImage form').submit(addModImage);
                    }
                    else
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        setTimeout(\"$('#dialogModImage').dialog('close') \",1000);

                        $('#modules').append(createDivImage(data.model.id, data.model.image));

                        //$('#modules').append(\"<li class='module mod-image' id='mod-\"+ data.model.id +\"'><div class='id'>\"+ data.model.id +\"</div><div class='title'>\"+ data.model.image +\"</div></li>\");
                    }
     
                } ",
                ))?>;
        return false; 
    }

    // -- Prepare udpate
    function prepareUpdateModImage(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modimage/update/'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'get',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        $('#Modimage_ref_project').val($('#project-id').html());
                        $('#dialogModImage div.divDialogFormImage form').submit(updateModImage);
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Update
    function updateModImage()
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modimage/update/0'),
                'data'=> "js:$(this).serialize() +'&id='+ $(\"#currentModuleId\").html()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        $('#Modimage_ref_project').val($('#project-id').html());
                        $('#dialogModImage div.divDialogFormImage form').submit(updateModImage);
                    }
                    else
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        setTimeout(\"$('#dialogModImage').dialog('close') \",1000);

                        $('#modules').find('#mod-image-'+ data.model.id +'').find('.module-content-inner').html(data.model.embeded_code);
                    }
     
                } ",
                ))?>;
        return false; 
    }
    */

    function addModImage()
    {
        console.log("Create mode image");

        var formData = new FormData($("#modimage-form")[0])

        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Modimage/create"); ?>',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        console.log("failure");
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        console.log($('#project-id').html());
                        $('#Modimage_ref_project').val($('#project-id').html());
                        $('#Modimage_order_elt').val($('.module').length + 1);
                    }
                }
            }
        );

        return false; 
    }

    function prepareUpdateModImage(id)
    {
        console.log("Prepare for id : "+ id);
        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Modimage/update/"); ?>/'+ id,
                type: 'POST',
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        $('#dialogModImage div.divDialogFormImage form').submit(updateModImage(id));
                    }
                }
            }
        );

        return false; 
    }

    // -- Update
    function updateModImage(id)
    {
        console.log("Update id : "+ id);

        var formData = new FormData($("#modimage-form")[0])

        $.ajax( 
            {
                url: '<?php echo Yii::app()->createUrl("Modimage/update/"); ?>/'+ id,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        console.log("failure");
                        //$('#dialogImage div.divDialogForm form').submit(updateImage(id));
                    }
                    else
                    {
                        console.log("succes");
                        $('#dialogModImage div.divDialogFormImage').html(data.div);
                        setTimeout(function() {
                            $('#dialogModImage').dialog('close')
                        }, 1000);
                        $('.image-'+ data.model.id +'').html(data.model.value);
                    }

                }
            }
        );

        return false; 
    }

    // -- Delete
    function deleteModImage(id)
    {
        <?php echo CHtml::ajax(array(
                'url'=>array('Modimage/delete/0'),
                'data'=>'js:{id:$("#currentModuleId").html()}',
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'success')
                    {
                        $('#modules').find('#mod-image-'+ data.model.id +'').fadeOut();
                    }
     
                } ",
                ))?>;
        return false; 
     
    }

    // -- Open update
    $('#modules').on('click', '.mod-image .module-edit', function() {
        var id = $(this).parents('.module').attr('mod-id');
        console.log("ID : "+ id);

        $('#currentModuleId').html(id);

        updateModImage(id);
        $('#dialogModImage').dialog('open');
    });

    function createDivImage(id, image) {
        return '<div class="module mod-image" id="mod-image-'+ id +'" mod-id="'+ id +'"><div class="module-header"><div class="module-type">Image</div><div class="module-delete">delete</div><div class="clear"></div></div><div class="module-content">'+ image +'</div></div>';
    }

    // Click to delete
    $('#modules').on('click', '.module-delete', function() {
        //.mod-title 
        var module = $(this).parents('.module');
        var id = module.attr('mod-id');
        console.log("ID delete : "+ id);

        $('#currentModuleId').html(id);

        if(module.hasClass('mod-title')) {
            console.log("Delete title");
            deleteModTitle(id);
        }
        if(module.hasClass('mod-text')) {
            console.log("Delete text");
            deleteModText(id);
        }
        if(module.hasClass('mod-video')) {
            console.log("Delete video");
            deleteModVideo(id);
        }
        if(module.hasClass('mod-slider')) {
            console.log("Delete slider");
            deleteModSlider(id);
        }
        if(module.hasClass('mod-image')) {
            console.log("Delete image");
            deleteModImage(id);
        }

        setTimeout(function() {
            updateModuleOrders();
        }, 400);
    });
			    
</script>
