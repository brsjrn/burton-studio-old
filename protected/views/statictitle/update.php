<?php
/* @var $this StatictitleController */
/* @var $model Statictitle */

$this->breadcrumbs=array(
	'Statictitles'=>array('admin'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);
?>

<div id="top_admin_model">
	<h1>Update Statictitle #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('back', array('Statictitle/admin')); ?></span></h1>
	
	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">save</div>
		<?php echo CHtml::link('', array('Statictitle/view', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-eye-open')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('Statictitle/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?></div>