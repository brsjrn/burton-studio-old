<?php
/* @var $this StatictitleController */
/* @var $model Statictitle */

$this->breadcrumbs=array(
	'Statictitles'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Statictitle', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#statictitle-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Statictitles</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Statictitle/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'statictitle-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'getTitle',
		'value',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>