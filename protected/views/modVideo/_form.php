<?php
/* @var $this ModimageController */
/* @var $model Modimage */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'modimage-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>


	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row hidden_row">
		<?php echo $form->labelEx($model,'source'); ?>
		<?php echo $form->textField($model,'source'); ?>
		<?php echo $form->error($model,'source'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'embeded_code'); ?>
		<?php echo $form->textField($model,'embeded_code'); ?>
		<?php echo $form->error($model,'embeded_code'); ?>
	</div>

	<div class="row hidden_row">
		<?php echo $form->labelEx($model,'ref_project'); ?>
		<?php echo $form->textField($model,'ref_project'); ?>
		<?php echo $form->error($model,'ref_project'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image_cover'); ?>
		<?php echo $form->fileField($model,'image_cover',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'image_cover'); ?>
		<div class="clear"></div>
		
		<?php if(!$model->isNewRecord && $model->image_cover != ''){ ?>
	        <div class='image_already_exist'>
             	<?php echo CHtml::image('../../../images/Modvideo/' . $model->image_cover,'image',array('class'=>'image_preview')); ?>
	        </div>
    	<?php } ?>
	</div>


	<div class="row hidden_row">
		<?php echo $form->labelEx($model,'order_elt'); ?>
		<?php echo $form->textField($model,'order_elt'); ?>
		<?php echo $form->error($model,'order_elt'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
	
	<!--
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>
	-->

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
