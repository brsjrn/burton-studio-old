<?php
/* @var $this ModvideoController */
/* @var $data Modvideo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('source')); ?>:</b>
	<?php echo CHtml::encode($data->source); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('embeded_code')); ?>:</b>
	<?php echo CHtml::encode($data->embeded_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_cover')); ?>:</b>
	<?php echo CHtml::encode($data->image_cover); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ref_project')); ?>:</b>
	<?php echo CHtml::encode($data->ref_project); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_elt')); ?>:</b>
	<?php echo CHtml::encode($data->order_elt); ?>
	<br />


</div>