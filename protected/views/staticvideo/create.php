<?php
/* @var $this StaticvideoController */
/* @var $model Staticvideo */

$this->breadcrumbs=array(
	'Staticvideos'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Staticvideo', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Create Staticvideo<span class="back_admin"><?php echo CHtml::link('back', array('Staticvideo/admin')); ?></span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?></div>