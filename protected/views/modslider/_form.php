<?php
/* @var $this ModsliderController */
/* @var $model Modslider */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'modslider-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">You are going to create a new slider.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row hidden_row">
		<?php echo $form->labelEx($model,'ref_project'); ?>
		<?php echo $form->textField($model,'ref_project'); ?>
		<?php echo $form->error($model,'ref_project'); ?>
	</div>

	<div class="row hidden_row">
		<?php echo $form->labelEx($model,'order_elt'); ?>
		<?php echo $form->textField($model,'order_elt'); ?>
		<?php echo $form->error($model,'order_elt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->