<?php
/* @var $this ContactController */
/* @var $data Contact */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adress_line_1')); ?>:</b>
	<?php echo CHtml::encode($data->adress_line_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adress_line_2')); ?>:</b>
	<?php echo CHtml::encode($data->adress_line_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adress_country')); ?>:</b>
	<?php echo CHtml::encode($data->adress_country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />


</div>