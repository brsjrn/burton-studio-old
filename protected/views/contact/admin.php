<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs=array(
	'Contacts'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Contact', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#contact-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Contacts</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Contact/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contact-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'adress_line_1',
		'adress_line_2',
		'adress_country',
		'phone',
		'email',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>


	<?php
	// Dialog text
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
	    'id'=>'dialogSeo',
	    'options'=>array(
	        'title'=>'Edit SEO',
	        'autoOpen'=>false,
	        'modal'=>true,
	        'width'=>550,
	        'height'=>470,
	    ),
	));?>
	<div class="divDialogForm"></div>  
	<?php $this->endWidget();?>

	<div class="admin-bloc">

	    <!-- ABOUT SEO -->
	    <?php if(isset($fields['contact_seo'])) { ?>
	    <div class="elt_static">
	        <div class="elt_static_label"><?php echo $fields['contact_seo']->title; ?></div>
	        <div class="elt_static_value seo-<?php echo $fields['contact_seo']->id; ?>"><?php echo $fields['contact_seo']->value; ?></div>

	        <?php echo CHtml::link('Edit', "",  // the link for open the dialog
	            array(
	                'elt-id'=>$fields['contact_seo']->id,
	                'class'=>'elt_static_seo_edit',
	                'style'=>'cursor: pointer; text-decoration: underline;',
	            ));
	        ?>

	        <div class="clear"></div>
	    </div>
	    <?php } ?>
	</div>

	<div id="selectedEltId" class="hidden_data"></div>

</div>

<script type="text/javascript">
	
	/* STATIC SEO */
	// -- Update
	function updateSeo()
	{
	    <?php echo CHtml::ajax(array(
	        'url'=>array('Staticseo/update/0'),
	        'data'=> "js:$(this).serialize() +'&id='+ $(\"#selectedEltId\").html()",
	        'type'=>'post',
	        'dataType'=>'json',
	        'success'=>"function(data)
	        {
	            if (data.status == 'failure')
	            {
	                console.log('fail...');
	                $('#dialogSeo div.divDialogForm').html(data.div);
	                $('#dialogSeo div.divDialogForm form').submit(updateSeo);
	            }
	            else
	            {
	                console.log('success');
	                $('#dialogSeo div.divDialogForm').html(data.div);
	                setTimeout(\"$('#dialogSeo').dialog('close') \",1000);

	                $('.seo-'+ data.model.id +'').html(data.model.value);
	            }

	        } ",
	        ))?>;
	    return false; 
	}

	// -- Update seo
	$('.elt_static_seo_edit').on('click', function() {
	    var id = $(this).attr('elt-id');
	    console.log("ID : "+ id);

	    $('#selectedEltId').html(id);

	    updateSeo(id);
	    $('#dialogSeo').dialog('open');
	});

</script>