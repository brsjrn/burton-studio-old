<?php
/* @var $this StaticsliderimageController */
/* @var $model Staticsliderimage */

$this->breadcrumbs=array(
    'About'=>array('site/aboutAdmin/'),
    'Staticsliderimage'=>array('adminById', 'refModelId'=>$parent->id),
    'Add image',
    //'Dates'=>array('adminById', 'refModelId'=>$evenement->id)
);

$this->menu=array(
	array('label'=>'Manage slider images', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Create slider images<span class="back_admin"><?php echo CHtml::link('retour', array('Staticsliderimage/adminById', 'refModelId'=>$parent->id)); ?></span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model, 'parent'=>$parent)); ?></div>