<?php
/* @var $this StaticsliderimageController */
/* @var $model Staticsliderimage */

$this->breadcrumbs=array(
	'About'=>array('site/aboutAdmin/'),
	'Staticsliderimage'=>array('adminById', 'refModelId'=>$parent->id),
	'Update',
);
?>

<div id="top_admin_model">
	<h1>Update slider image #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('retour', array('Staticsliderimage/adminById', 'refModelId'=>$parent->id)); ?></span></h1>
	
	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">save</div>
        <?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>
        <?php echo CHtml::link('', array('Staticsliderimage/create', 'refModelId'=>$parent->id), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>
        <div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>
<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model, 'parent'=>$parent)); ?></div>