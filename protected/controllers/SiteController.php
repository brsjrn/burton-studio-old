<?php

class SiteController extends Controller
{
        public $layout = 'column1';


	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// Set user language to FR
		Yii::app()->language='fr';

		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}


	public function actionAboutAdmin()
	{
		$fields = $this->getAboutFields();
		$nbSliderImage = 0;

		$criteria1 = new CDbCriteria();
		$criteria1->addCondition("getTitle=:getTitle");
		$criteria1->params = array(
		    ':getTitle'=>'about_slider_1'
		);
		$slider1 = Staticslider::model()->find($criteria1);

		if(isset($slider1)) {
			$criteria2 = new CDbCriteria();
			$criteria2->addCondition("ref_slider=:ref_slider");
			$criteria2->params = array(
			    ':ref_slider'=>$slider1->id
			);
			$imagesSlider = Staticsliderimage::model()->findAll($criteria2);

			$nbSliderImage = count($imagesSlider);
		}

		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('admin-about', array(
			'fields' => $fields,
			'nbSliderImage' => $nbSliderImage
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;
                
                $this->layout = 'column0';

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	// Get about fields
	protected function getAboutFields() {

    	// About video 1
	    $criteria11 = new CDbCriteria();
	    $criteria11->addCondition("getTitle=:getTitle");
	    $criteria11->params = array(
	        ':getTitle'=>'about_video_1'
	    );
	    $fields['about_video_1'] = Staticvideo::model()->find($criteria11);

		// About title 1
	    $criteria1 = new CDbCriteria();
	    $criteria1->addCondition("getTitle=:getTitle");
	    $criteria1->params = array(
	        ':getTitle'=>'about_title_1'
	    );
	    $fields['about_title_1'] = Statictitle::model()->find($criteria1);

	    // About text 1
        $criteria4 = new CDbCriteria();
        $criteria4->addCondition("getTitle=:getTitle");
        $criteria4->params = array(
            ':getTitle'=>'about_text_1'
        );
        $fields['about_text_1'] = Statictext::model()->find($criteria4);

    	// About image 1
	    $criteria7 = new CDbCriteria();
	    $criteria7->addCondition("getTitle=:getTitle");
	    $criteria7->params = array(
	        ':getTitle'=>'about_image_1'
	    );
	    $fields['about_image_1'] = Staticimage::model()->find($criteria7);

    	// About image 2
	    $criteria8 = new CDbCriteria();
	    $criteria8->addCondition("getTitle=:getTitle");
	    $criteria8->params = array(
	        ':getTitle'=>'about_image_2'
	    );
	    $fields['about_image_2'] = Staticimage::model()->find($criteria8);

    	// About title 2
	    $criteria2 = new CDbCriteria();
	    $criteria2->addCondition("getTitle=:getTitle");
	    $criteria2->params = array(
	        ':getTitle'=>'about_title_2'
	    );
	    $fields['about_title_2'] = Statictitle::model()->find($criteria2);

	    // About text 2
        $criteria5 = new CDbCriteria();
        $criteria5->addCondition("getTitle=:getTitle");
        $criteria5->params = array(
            ':getTitle'=>'about_text_2'
        );
        $fields['about_text_2'] = Statictext::model()->find($criteria5);

    	// About Slider
	    $criteria10 = new CDbCriteria();
	    $criteria10->addCondition("getTitle=:getTitle");
	    $criteria10->params = array(
	        ':getTitle'=>'about_slider_1'
	    );
	    $fields['about_slider_1'] = Staticslider::model()->find($criteria10);

    	// About title 3
        $criteria3 = new CDbCriteria();
        $criteria3->addCondition("getTitle=:getTitle");
        $criteria3->params = array(
            ':getTitle'=>'about_title_3'
        );
        $fields['about_title_3'] = Statictitle::model()->find($criteria3);

    	// About text 3
        $criteria6 = new CDbCriteria();
        $criteria6->addCondition("getTitle=:getTitle");
        $criteria6->params = array(
            ':getTitle'=>'about_text_3'
        );
        $fields['about_text_3'] = Statictext::model()->find($criteria6);

    	// About image 3
	    $criteria9 = new CDbCriteria();
	    $criteria9->addCondition("getTitle=:getTitle");
	    $criteria9->params = array(
	        ':getTitle'=>'about_image_3'
	    );
	    $fields['about_image_3'] = Staticimage::model()->find($criteria9);

	    // About SEO
        $criteria10 = new CDbCriteria();
        $criteria10->addCondition("getTitle=:getTitle");
        $criteria10->params = array(
            ':getTitle'=>'about_seo'
        );
        $fields['about_seo'] = Staticseo::model()->find($criteria10);

	    return $fields;
	}

}