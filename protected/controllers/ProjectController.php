<?php

class ProjectController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Project;

		$listOrders = $this->getListOrderCreate();
					
		
		// KCFinder config
        $_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder
	
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];

			// Manage order
			
			if($model->ordre <= $this->countModels()) {
                // On récupère toutes les rubriques dont l'ordre est >=
                $listModels = $this->getModelsSupOrdre($model->ordre);
                
                foreach ($listModels as $oldModel) {
                    $oldModel->ordre += 1;
                    $oldModel->save();
                }
            }
						
			// Manage links
						

			// Manage images upload
			
			$tmp_stared_cover_image = CUploadedFile::getInstance($model, 'stared_cover_image');

			if(isset($tmp_stared_cover_image)) {
                $model->stared_cover_image = 'Project'.'_stared_cover_image'. uniqid() .'.'. $tmp_stared_cover_image->extensionName;
            }
		
			$tmp_cover_image = CUploadedFile::getInstance($model, 'cover_image');

			if(isset($tmp_cover_image)) {
                $model->cover_image = 'Project'.'_cover_image'. uniqid() .'.'. $tmp_cover_image->extensionName;
            }
						            
			if($model->save()) {
				
				if(isset($tmp_stared_cover_image)) {
					if(!$tmp_stared_cover_image->saveAs('images/Project/'. $model->stared_cover_image)) {
                         throw new ExceptionClass('Problem Project image upload');
                     }
				}
			
				if(isset($tmp_cover_image)) {
					if(!$tmp_cover_image->saveAs('images/Project/'. $model->cover_image)) {
                         throw new ExceptionClass('Problem Project image upload');
                     }
				}
				
				/*$this->redirect('update',array(
					'model' => $model,
					'listOrders' => $listOrders
				));*/

				$this->redirect(array('update','id'=>$model->id));
			}
		} else {
			$this->render('create',array(
				'model' => $model,
				'listOrders' => $listOrders
			));
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$listOrders = $this->getListOrderUpdate();
		$oldOrdre = $model->ordre;

		
		// KCFinder config
        $_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder

	
		// Current images
		
		$old_stared_cover_image = $model->stared_cover_image;
	
		$old_cover_image = $model->cover_image;
					
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];

			// Manage order
			
			// Si le nouveau rank est supérieur
            if($model->ordre > $oldOrdre) {
                // On récupère toutes les rubriquesdont l'ordre est compris entre oldOrdre et model->ordre
                $listModels = $this->getModelsBetweenSupOrdres($oldOrdre, $model->ordre);
                
                for($i=0; $i<$model->ordre-$oldOrdre; $i++) {
                    $oldModel = $listModels[$i];
                    $oldModel->ordre -= 1;
                    $oldModel->save();
                }
            }
            // Si le nouveau rank est inférieur
            if($model->ordre < $oldOrdre) {
                // On récupère toutes les rubiques dont l'ordre est compris entre model->ordre et oldOrdre
                $listModels = $this->getModelsBetweenInfOrdres($model->ordre, $oldOrdre);
                
                for($i=0; $i<$oldOrdre-$model->ordre; $i++) {
                    $oldModel = $listModels[$i];
                    $oldModel->ordre += 1;
                    $oldModel->save();
                }
            }
						
			// Manage links
			
			// Manage images upload
			
			$tmp_stared_cover_image = CUploadedFile::getInstance($model, 'stared_cover_image');

			if(isset($tmp_stared_cover_image)) {
                $model->stared_cover_image = 'Project'.'_stared_cover_image'. uniqid() .'.'. $tmp_stared_cover_image->extensionName;
            }
            if(!isset($tmp_stared_cover_image) && isset($old_stared_cover_image)) {
                $model->stared_cover_image = $old_stared_cover_image;
            }
		
			$tmp_cover_image = CUploadedFile::getInstance($model, 'cover_image');

			if(isset($tmp_cover_image)) {
                $model->cover_image = 'Project'.'_cover_image'. uniqid() .'.'. $tmp_cover_image->extensionName;
            }
            if(!isset($tmp_cover_image) && isset($old_cover_image)) {
                $model->cover_image = $old_cover_image;
            }
						
			if($model->save()) {
				
				if(isset($tmp_stared_cover_image)) {
					if(!$tmp_stared_cover_image->saveAs('images/Project/'. $model->stared_cover_image)) {
                         throw new ExceptionClass('Problem Project image upload');
                     }
                     if(isset($old_stared_cover_image) && strlen($old_stared_cover_image)>0) {
                        unlink('images/Project/' . $old_stared_cover_image);
                    }
				}
			
				if(isset($tmp_cover_image)) {
					if(!$tmp_cover_image->saveAs('images/Project/'. $model->cover_image)) {
                         throw new ExceptionClass('Problem Project image upload');
                     }
                     if(isset($old_cover_image) && strlen($old_cover_image)>0) {
                        unlink('images/Project/' . $old_cover_image);
                    }
				}
				
				$this->redirect(array('admin'));
			}
		}

		// Get modules
		$orderedModules = $this->getOrderedModules($model->id);

		
		$this->render('update',array(
			'model' => $model,
			'listOrders' => $listOrders,
			/*'listModTitles' => $listModTitles,
			'listModTexts' => $listModTexts,
			'listModVideos' => $listModVideos,
			'listModSliders' => $listModSliders,
			'listModImages' => $listModImages*/
			'orderedModules' => $orderedModules
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();

		 
		$nbModels = $this->countModels()+1;

		// On récupère toutes les rubriques dont l'ordre est compris entre oldOrdre et model->ordre
        $listModels = $this->getModelsBetweenSupOrdres($model->ordre, $nbModels);

        for($i=0; $i<$nbModels-$model->ordre; $i++) {
            $oldModel = $listModels[$i];
            $oldModel->ordre -= 1;
            $oldModel->save();
        }
	



		if(isset($model->stared_cover_image) && $model->stared_cover_image != '') {
            unlink('images/Project/'. $model->stared_cover_image);
        }
	
		if(isset($model->cover_image) && $model->cover_image != '') {
            unlink('images/Project/'. $model->cover_image);
        }
					        

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Project');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Project the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Project::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Project $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = Project::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = Project::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = Project::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = Project::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Project::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Project::model()->findAll($criteria);
        
        return $listModels;
    }

    protected function getOrderedModules($id)
    {
    	$orderedModules = array();

    	// -- Titles
    	$criteria1 = new CDbCriteria();
    	$criteria1->addCondition('ref_project=:ref_project');
    	$criteria1->params = array(
    	        ':ref_project'=>$id
    	);
    	$listModTitles = Modtitle::model()->findAll($criteria1);

    	// -- Titles
    	$criteria2 = new CDbCriteria();
    	$criteria2->addCondition('ref_project=:ref_project');
    	$criteria2->params = array(
    	        ':ref_project'=>$id
    	);
    	$listModTexts = Modtext::model()->findAll($criteria2);

    	// -- Titles
    	$criteria3 = new CDbCriteria();
    	$criteria3->addCondition('ref_project=:ref_project');
    	$criteria3->params = array(
    	        ':ref_project'=>$id
    	);
    	$listModVideos = Modvideo::model()->findAll($criteria3);

    	// -- Titles
    	$criteria4 = new CDbCriteria();
    	$criteria4->addCondition('ref_project=:ref_project');
    	$criteria4->params = array(
    	        ':ref_project'=>$id
    	);
    	$listModSliders = Modslider::model()->findAll($criteria4);

    	// -- Titles
    	$criteria5 = new CDbCriteria();
    	$criteria5->addCondition('ref_project=:ref_project');
    	$criteria5->params = array(
    	        ':ref_project'=>$id
    	);
    	$listModImages = Modimage::model()->findAll($criteria5);

    	
    	foreach ($listModTitles as $key => $modTitle) {
    		$cpt = 0;

    		array_push($orderedModules, $modTitle);
    	}

    	foreach ($listModTexts as $key => $modText) {
    		$cpt = 0;

    		array_push($orderedModules, $modText);
    	}

    	foreach ($listModVideos as $key => $modVideo) {
    		$cpt = 0;

    		array_push($orderedModules, $modVideo);
    	}

    	foreach ($listModSliders as $key => $modSlider) {
    		$cpt = 0;

    		array_push($orderedModules, $modSlider);
    	}

    	foreach ($listModImages as $key => $modImage) {
    		$cpt = 0;

    		array_push($orderedModules, $modImage);
    	}
    	
    	// Sort array
    	usort($orderedModules, function($a, $b) {
    	    return $a->order_elt - $b->order_elt;
    	});

    	return $orderedModules;
    }
}
